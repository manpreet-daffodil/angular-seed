import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {AppSettings} from '../config/config';


// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

// Import model for strict casting
import { Register } from '../model/register.model';

@Injectable()
export class RegisterService {
   private baseUrl : string;
   // Resolve HTTP using the constructor
  constructor(private http: Http) { 

     // private instance variable to hold base url
     this.baseUrl = `${AppSettings.API_ENDPOINT}/signup`; 

  }

   // @Post Register
    postRegister (body: Object): Observable<Register[]> {
    	console.log('body',body)
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option

        return this.http.post( this.baseUrl , bodyString , options) // ...using post request
                         .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw(error.json().message || 'Server error')); //...errors if any
    } 


}
