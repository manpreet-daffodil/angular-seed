import { Component, OnInit } from '@angular/core';
import { RegisterService } from './register.service';
import { Service } from '../service/http-request.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../login/login.component.css']
})
export class RegisterComponent implements OnInit {
  public signup = {};
  public errorMessage = "";
  public pwpattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
  public passwordStrength = false;
  public registeration = false;
  public sucessname = '';
  constructor( 
    private registerService : RegisterService,
    private httpservice : Service,
    private router : Router
  ) { }

  ngOnInit() {
  
  }

  register() {
     this.httpservice.postRegister(this.signup).subscribe((data) => {
      if(data) {
        this.sucessname = data["firstName"];
        this.registeration = true; 
      }
    },(err) => {
      this.errorMessage = err;
    })
  }

  checkPasswordLength(password) {
     return this.passwordStrength = this.pwpattern.test(password); 
  }


}
