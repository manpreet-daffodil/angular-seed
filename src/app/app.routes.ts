import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginRouteGuard } from './route-guard/login-guard';
import { AuthGuard } from './route-guard/auth-guard';
export const routes: Routes = [
  
  {
  	path: 'login',
    component: LoginComponent,
    canActivate: [AuthGuard]
  },

  {
  	path: '',
    component: LoginComponent,
    canActivate: [AuthGuard]
  },

  {
  	path: 'register',
    component: RegisterComponent
  },

  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [LoginRouteGuard]
  }
  
  // { 
  //   path: 'forgotpassword', 
  //   loadChildren: 'app/forgotpassword/forgotpassword.module#ForgotpasswordModule' 
  // }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
