import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'angular2-cookie/core';
import { SocketService }       from '../service/socket.service';
import { LoginService } from '../login/login.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  private messages = [];
  private insert_msg:any;
  connection;
  message;
  
  constructor(
  	private router : Router,
    private loginservice: LoginService,
  	private _cookieService:CookieService,
     private socketService:SocketService
  ) {
      console.log("idhjhdfvbhfv",this.messages);
      
      this.insert_msg = [];

  }
  
  logout() {
    this.messages = [];
  	this._cookieService.removeAll();
  	this.router.navigate(['/login']);  
  }
  
  sendMessage(){
    this.socketService.sendMessage(this.message);
    this.message = '';
  }

  ngOnInit() {
    
    this.socketService.uservalidate();
    this.connection = this.socketService.getnotifications().subscribe(message => {
      console.log("only msg",message)
      console.log("this.insert_msg",this.messages)
      
      this.messages = this.messages.concat(message)

      //this.messages.push(message);
      console.log('After message =',this.messages);
    })
  }
}
