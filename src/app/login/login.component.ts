import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { CookieService } from 'angular2-cookie/core';
import { SocketService }       from '../service/socket.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
   public user  = {};
   public errorMessage = "";
  constructor(
    private loginService : LoginService,
    private router : Router,
    private _cookieService:CookieService,
    private socketService:SocketService
      ) { }

  ngOnInit() {
    
  }

  submitLogin() {
    this.loginService.postLogin(this.user).subscribe((data) => {
      console.log("data =",data);
      if(data) {
        this.loginService.setUser(data);
        console.log("cookie data =",this._cookieService.getObject('user'));
        this.socketService.uservalidate();
        this.socketService.sendMessage(data);
        this.router.navigate(['/dashboard']);  
      }
    },(err) => {
      console.log('err',err); 
      this.errorMessage = err;
    })
  }

  changeUsername(value) {
    this.errorMessage = "";
  }
}
