import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {CookieService} from 'angular2-cookie/core';
import {AppSettings} from '../config/config';
/*To install socket io you have to run this command

  npm install @types/socket.io-client --save

*/
import * as io from "socket.io-client";
@Injectable()
export class SocketService {
  private url = `${AppSettings.API_ENDPOINT}`;  
  private socket;
  private state;
  
  constructor(private _cookieService:CookieService){

    this.state={socket:io(`${AppSettings.API_ENDPOINT}`)}
  }

  sendMessage(message){
    console.log("emit ",message);
    // this.socket = io(this.url);
    this.socket.emit('add-message', message);    
  }
  
  uservalidate(){
    console.log("uservalidate function");
    this.socket.emit('user-validate',this._cookieService.getObject('user'));
  }
  
  getPosts() {
    let observable = new Observable(observer => {
      this.socket = io(this.url);
      this.socket.on('addPost', (data) => {
        console.log("yes inside post-=-=-=/////",data);
        observer.next(data);    
      });
      return () => {
        console.log("disconnected socket.io");
        this.socket.disconnect();
      };  
    })     
    return observable;
  }



  getnotifications() {
    console.log("In getnotifications");
    let observable = new Observable(observer => {
<<<<<<< HEAD
      console.log("this.socket",this.socket);
      //this.socket = io(this.url);
      console.log("After this.socket",this.socket);
=======
      this.socket = io(this.url);
      console.log('this.socket',this.socket);
>>>>>>> himanshu
      this.socket.on('get-notification', (data) => {
        console.log("yes inside post-=-=-= '''",data);
        observer.next(data);    
      });
      return () => {
        console.log("disconnected socket.io");
        this.socket.disconnect();
      };  
    })     
    return observable;
  }

  getMessages() {
    let observable = new Observable(observer => {
      this.socket = io(this.url);
      this.socket.on('welcome', (data) => {
        console.log("yes inside message-=-=-=",data);
        observer.next(data);    
      });
      // this.socket.on('post', (data) => {
      //   console.log("yes inside post-=-=-=",data);
      //   observer.next(data);    
      // });
      return () => {
        console.log("disconnected socket.io");
        this.socket.disconnect();
      };  
    })     
    return observable;
  }
      
}

