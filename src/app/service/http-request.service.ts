// Imports
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {CookieService} from 'angular2-cookie/core';
import {AppSettings} from '../config/config';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

// Import model for strict casting
import { Login } from '../model/login.model';
import { Register } from '../model/register.model';

@Injectable()
export class Service {
   private baseUrl : string;
   private isuser;
   private headers:any;
   private options:any;
  // Resolve HTTP using the constructor
  constructor(
    private http: Http,
    private _cookieService:CookieService
  ) { 
   //this.baseUrl = 'http://localhost:8000/login';
   this.headers = new Headers({ 'Content-Type': 'application/json' });
   this.options = new RequestOptions({ headers: this.headers }); 
  }
  
  setUser(data) {
    this._cookieService.putObject("user",{
      _id:data['_id'], 
      email:data['local']["email"],
      firstName:data['local']["firstName"],
      lastName:data['local']["lastName"]
    });
  }

  getUser() {
    return this._cookieService.getObject("user");
  }
   // @Post Login
    postLogin (body: Object): Observable<Login[]> {
        let bodyString = JSON.stringify(body); // Stringify payload
        //let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
       // let options       = new RequestOptions({ headers: headers }); // Create a request option
       this.baseUrl = `${AppSettings.API_ENDPOINT}/login`;
        return this.http.post( this.baseUrl , bodyString , this.options) // ...using post request
                         .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw(error.json().message || 'Server error')); //...errors if any
    } 
   
   postRegister (body: Object): Observable<Register[]> {
      console.log('body',body)
        let bodyString = JSON.stringify(body); // Stringify payload
        //let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        //let options       = new RequestOptions({ headers: headers }); // Create a request option
        this.baseUrl = `${AppSettings.API_ENDPOINT}/register`; 
        return this.http.post( this.baseUrl , bodyString , this.options) // ...using post request
                         .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw(error.json().message || 'Server error')); //...errors if any
    }


}
