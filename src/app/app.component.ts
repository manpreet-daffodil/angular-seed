import { Component , OnInit ,OnDestroy } from '@angular/core';
import { SocketService }       from './service/socket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit,OnDestroy {
  public title = 'app works!';
  public messages = [];
  public connection;
  public message;
  // private socket;

  constructor( private socketService:SocketService ) {}

  sendMessage(){
    this.socketService.sendMessage(this.message);
    this.message = '';
  }

  ngOnInit() {
    this.connection = this.socketService.getMessages().subscribe(message => {
      console.log('Msg here',message);
      this.messages.push(message);
    })
  }
  
  ngOnDestroy() {
    this.connection.unsubscribe();
  }

}
