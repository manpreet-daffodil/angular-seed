import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login/login.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private loginService : LoginService) { }

  ngOnInit() {
  }

  checkuser() {
    this.loginService.findMe().subscribe((data) => {
      console.log("find me user data fetch",data);
      
    },(err) => {
      console.log('err',err); 
    })
  }

}
