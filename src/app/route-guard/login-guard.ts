import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Service } from '../service/http-request.service';

@Injectable()
export class LoginRouteGuard implements CanActivate {

 constructor (
 	private router : Router,
 	private httpservice : Service
 ) {}

 canActivate() {
     if(!this.httpservice.getUser()) this.router.navigate(['/login']);
   	 return true;
 }
}