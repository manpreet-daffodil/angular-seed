import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Service } from '../service/http-request.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

 constructor (
 	private router : Router,
 	private httpservice : Service
 ) {}

 canActivate() {
     if(this.httpservice.getUser()) this.router.navigate(['/dashboard']);
   	 return true;
 }
}